# grafx2

A bitmap paint program specializing in 256-color graphics with a large number of tools and effects.

# Contributing

GRAFX2 is maintained at https://github.com/deverac/grafx2-dos


## GRAFX2.LSM

<table>
<tr><td>title</td><td>grafx2</td></tr>
<tr><td>version</td><td>2.2</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-03-21</td></tr>
<tr><td>description</td><td>A graphics editor</td></tr>
<tr><td>keywords</td><td>freedos grafx2</td></tr>
<tr><td>summary</td><td>A bitmap paint program specializing in 256-color graphics with a large number of tools and effects.</td></tr>
<tr><td>maintained&nbsp;by</td><td>Devin Racher</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/deverac/grafx2-dos</td></tr>
<tr><td>original&nbsp;site</td><td>http://grafx2.chez.com/index.php</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, version 2.0](LICENSE)</td></tr>
</table>
